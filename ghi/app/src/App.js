import Nav from './Nav';
import React from 'react'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import { BrowserRouter, Link } from "react-router-dom";
import { Route, Routes } from 'react-router-dom';

export default function App(props) {
  if (props.attendees===undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav /> 
        <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="attendees">
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
        </div>
    </BrowserRouter>
  );
}

